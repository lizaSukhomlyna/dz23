package calculator;

import java.util.List;
import java.util.Map;

public interface DailyValueCalculator {
    Double calculateValueCurrency(List<Map<String, String>> data, List<String> periodTime);
}
