package calculator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class PathsGenerator {
    private LetterCode letterCode;
    private Path pathForDailyData;

    public PathsGenerator(LetterCode letterCode) {
        this.letterCode = letterCode;
        this.pathForDailyData = Path.of("src", "main", "resources", "dailyValue", this.letterCode.toString());
    }

    public List<Path> getPathsForDataLoad() throws IOException {
        return Files.walk(
                Paths.get(String.valueOf(this.pathForDailyData)))
                .filter(Files::isRegularFile)
                .collect(Collectors.toList());
    }
}
