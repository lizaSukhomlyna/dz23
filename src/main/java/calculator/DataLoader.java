package calculator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public interface DataLoader {
    List<Map<String, String>> loadData(List<Path> paths) throws IOException;
}
