package calculator;

import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class ProgramManager {
    private LetterCode letterCode;
    private Date bottomBoundDate;
    private Date upperBoundDate;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");


    public void start() {
        setData();
        calculateValueUAH();

    }

    private void setData() {
        do {
            System.out.println("Выбирете код валюты для вычисления средней стоимости одной единицы гривны");
            System.out.println("Доступные валюты:");
            System.out.println(Arrays.toString(LetterCode.values()));
            Scanner in = new Scanner(System.in);
            try {
                this.letterCode = LetterCode.valueOf(in.next());
                System.out.println("Введите нижнюю границу интересующего временного периода в формате: 01.06.2022");
                this.bottomBoundDate = dateFormat.parse(in.next());

                System.out.println("Введите верхнюю границу интересующего временного периода в формате: 01.06.2022");
                this.upperBoundDate = this.dateFormat.parse(in.next());
                break;
            } catch (Exception e) {
                System.out.println("Ошибка ввода.Попробуйте еще раз.");
            }
        } while (true);
    }

    private void calculateValueUAH() {
        try {

            List<String> periodTime = PeriodTime.getPeriodTime(upperBoundDate, bottomBoundDate);
            List<Path> paths = new PathsGenerator(letterCode).getPathsForDataLoad();
            DataProcessingFactory factory = new DataProcessingFactory();
            factory.newThread(new DataProcessingTask(periodTime, paths, letterCode));
            if (paths.size() <= 30) {
                factory.newThread(new DataProcessingTask(periodTime, paths, letterCode)).start();
            } else {
                int border = 30;
                int numFullBlockPaths = paths.size() / border;
                for (int i = 1; i <= numFullBlockPaths; i++) {
                    factory.newThread(new DataProcessingTask(periodTime, paths.subList((border * i) - 30, (border * i - 1)), letterCode)).start();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
