package calculator;

import java.text.SimpleDateFormat;
import java.util.*;

public class PeriodTime {

    public static List<String> getPeriodTime(Date upperBoundDate, Date bottomBoundDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        GregorianCalendar calendarBottomBound = new GregorianCalendar();
        calendarBottomBound.setTime(bottomBoundDate);
        GregorianCalendar calendarUpperBound = new GregorianCalendar();
        calendarUpperBound.setTime(upperBoundDate);

        List<String> dates = new ArrayList<>();
        dates.add(dateFormat.format(calendarBottomBound.getTime()));
        while (!calendarBottomBound.equals(calendarUpperBound)) {
            calendarBottomBound.add(Calendar.DAY_OF_MONTH, 1);
            dates.add(dateFormat.format(calendarBottomBound.getTime()));
        }

        dates
                .forEach(System.out::println);
        return dates;
    }
}
