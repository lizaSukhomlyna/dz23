package calculator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataLoaderFromFiles implements DataLoader {

    public List<Map<String, String>> loadData(List<Path> paths) throws IOException {
        List<Map<String, String>> listOfData = new ArrayList<>();

        for (Path filePath : paths) {
            Map<String, String> data = new HashMap<>();
            List<String[]> inform = parseDataFromFile(filePath);
            String[] firstLine = inform.get(0);
            String[] secondLine = inform.get(1);
            for (int i = 1; i < firstLine.length; i++) {
                data.put(firstLine[i].trim(), secondLine[i].trim());
            }
            listOfData.add(data);

        }
        return listOfData;
    }


    private List<String[]> parseDataFromFile(Path filePath) throws IOException {
        return Files.lines(filePath)
                .map(s -> s.split("\\|"))
                .collect(Collectors.toList());
    }


}
