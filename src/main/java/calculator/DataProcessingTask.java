package calculator;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class DataProcessingTask implements Runnable {
    List<String> periodTime;
    List<Path> paths;
    LetterCode letterCode;


    public DataProcessingTask(List<String> periodTime, List<Path> paths, LetterCode letterCode) {
        this.periodTime = periodTime;
        this.paths = paths;
        this.letterCode = letterCode;
    }

    @Override
    public void run() {
        List<Map<String, String>> data = null;
        try {
            data = new DataLoaderFromFiles().loadData(paths);
            double mediumVal = new DailyValueUAHCalculator().calculateValueCurrency(data, periodTime);
            System.out.println(" Средняя стоимости UAH  к " + this.letterCode.toString() + "->" + mediumVal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
