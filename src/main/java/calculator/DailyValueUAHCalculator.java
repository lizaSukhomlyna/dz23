package calculator;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DailyValueUAHCalculator implements DailyValueCalculator {

    public Double calculateValueCurrency(List<Map<String, String>> data, List<String> periodTime) {
        Optional<Double> val = data.stream()
                .filter(map -> periodTime.contains(map.get("Date")))
                .map(map -> Double.valueOf(map.get("\"UAH\"")))
                .reduce((i, j) -> (i + j) / 2);
        val.ifPresent(System.out::println);

        return val.get();
    }
}
