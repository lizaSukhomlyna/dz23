package calculator;

import java.util.concurrent.ThreadFactory;

public class DataProcessingFactory implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r);
    }
}

